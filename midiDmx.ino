/*
    MIDI to DMX converter.
    Copyright (C) 2020-2022  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    This software is written for Arduino and CQRobot DMX Shield.

    Jumpers:  EN
              DE
              TX-io
              RX-io

    After uploading, it is advicable to use dualMocoLufa to turn Arduino
    into class-compliant MIDI device.

    Incoming CC messages are converted to DMX messages according their
    number and value. MIDI channel information is not used.

    DmxSimple library is used as we need hardware serial for MIDI,
    and DmxSimple don't use it for DMX output (like most other DMX libraries do.

*/

#define USE_MIDI_LIBRARY

#include <DmxSimple.h>

#ifdef USE_MIDI_LIBRARY
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();
#endif

void setup() {

#ifdef USE_MIDI_LIBRARY
  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleControlChange(midiCC);
  MIDI.setHandleNoteOn(printBuffer);
  MIDI.turnThruOff();
#else
  Serial.begin(31250);
#endif

  pinMode(2, OUTPUT); /* This is needed to enable the DMX shield. */
  digitalWrite(2, HIGH);

  pinMode(13, OUTPUT);

  DmxSimple.maxChannel(B01111111);
  DmxSimple.usePin(4);
  DmxSimple.begin(); // This line requires modified DmxSimple, you can comment it out on stock version.

}

#ifdef USE_MIDI_LIBRARY

void loop() {
  MIDI.read();
}

void midiCC(byte channel, byte number, byte value) {

  /*  Light weight scaling,
      we want value 0 => 0 and 127 => 255,
      so will just manually move 254 to 255.
  */

  if (value == 127) {
    DmxSimple.write(number, 255);
  } else {
    DmxSimple.write(number, value * 2);
  }
}

void printBuffer(byte channel, byte note, byte velocity) {
  uint8_t* b = DmxSimple.getInternalBuffer(); // This line requires modified DmxSimple
  for (int i = 0; i < 127; i++) {
    MIDI.sendControlChange(i + 1, b[i] / 2, 1);
  }
}

#else

byte msg[0];
byte m = 0;
void loop() {

  if (Serial.available()) {

    byte b = Serial.read();

    if ((b >> 7) != 0) {
      /* status */
      if ((b & 0xF0) == 0xB0) {
        msg[0] = b;   /* Not really required, as we just follow CC in OMNI. */
        m = 1;        /* CC received, let's wait for data. */
      } else {
        m = 0;        /* Something else, will wait for next status. */
      }
    } else {
      /* data */
      if (m == 0) return; /* Still waiting for status. */
      msg[m] = b;
      m++;
      if (m == 3) {
        /* Full message received, processing... */
        if (msg[2] == 127) {
          DmxSimple.write(msg[1], 255);
        } else {
          DmxSimple.write(msg[1], msg[2] * 2);
        }

        m = 1; /* Running status */
      }
    }
  }
}
#endif



